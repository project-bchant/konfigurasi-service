package com.bca.konfigurasi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class KonfigurasiServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(KonfigurasiServiceApplication.class, args);
	}
}
